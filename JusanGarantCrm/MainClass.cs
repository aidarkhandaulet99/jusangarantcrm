﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace JusanGarantCrm
{
    internal class Program
    {
        private const int LEGAL_INPUT_PARAM_LENGTH = 2;
        private const int INDIVIDUAL_INPUT_PARAM_LENGTH = 4;
        private const int INDIVIDUAL_INPUT_PARAM_LENGTH_WITHOUT_PATRONYMIC = 3;

        public static void Main(string[] args)
        {
            // Чтение файла
            List<Contractor> contractors = readFile();

            // Запись в файл
            writeToFile(contractors);


            List<string> individualsNames = new List<string>();
            List<string> companyNames = new List<string>();
            foreach (Contractor contractor in contractors)
            {
                if (contractor is IndividContractor)
                {
                    IndividContractor temp = (contractor as IndividContractor);
                    individualsNames.Add(temp.Name + " " + temp.Surname + " " + temp.Patronymic);
                }
                else
                {
                    LegalContactor temp = (contractor as LegalContactor);
                    companyNames.Add(temp.CompanyName);
                }
            }

            // вывод имен физлиц отсортированные
            List<string> individualsNamesSorted = individualsNames.OrderBy(q => q).ToList();
            Console.WriteLine("INDIVIDUALS NAMES:");
            foreach (var name in individualsNamesSorted)
            {
                Console.WriteLine(name);
            }


            // вывод названии компании отсортированные
            List<string> companyNamesSorted = companyNames.OrderBy(q => q).ToList();
            Console.WriteLine("COMPANY NAMES:");
            foreach (var name in companyNamesSorted)
            {
                Console.WriteLine(name);
            }
        }

        private static List<Contractor> readFile()
        {
            var result = new List<Contractor>();
            try
            {
                /*
                 *  Формат для ип:
                    ИП "Название ип" биин 
                    Формат для физлиц:
                    Имя Фамилия Отчество иин гендер
                    -------------------------------
                    Отчество может отсутствовать
                    Длина иина должна быть 12 символов
                    Название ип без пробела должна быть
                 */
                StreamReader sr = new StreamReader("input.txt");
                string line = sr.ReadLine();
                while (line != null)
                {
                    string[] data = line.Split();
                    switch (data.Length)
                    {
                        case LEGAL_INPUT_PARAM_LENGTH:
                            string companyName = data[0];
                            string biin = data[1];
                            if (isValidIin(biin))
                            {
                                Contractor contractor = new LegalContactor(companyName, biin);
                                result.Add(contractor);
                            }
                            else
                            {
                                Console.WriteLine("WRONG IIN " + biin);
                            }

                            break;
                        case INDIVIDUAL_INPUT_PARAM_LENGTH:
                            string name = data[0];
                            string surname = data[1];
                            string patronymic = data[2];
                            string iin = data[3];
                            if (isValidIin(iin))
                            {
                                Contractor contractor = new IndividContractor(name, surname, patronymic, iin);
                                result.Add(contractor);
                            }
                            else
                            {
                                Console.WriteLine("WRONG IIN " + iin);
                            }

                            break;
                        case INDIVIDUAL_INPUT_PARAM_LENGTH_WITHOUT_PATRONYMIC:
                            if (isValidIin(data[2]))
                            {
                                Contractor contractor = new IndividContractor(data[0], data[1], null, data[2]);
                                result.Add(contractor);
                            }
                            else
                            {
                                Console.WriteLine("WRONG IIN " + data[2]);
                            }

                            break;
                        default:
                            Console.WriteLine("DAMAGED INPUT DATA");
                            break;
                    }

                    line = sr.ReadLine();
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("FILE NOT FOUNT, PLEASE EDIT WORK DIRECTORY OF RIDER");
            }

            return result;
        }

        private static bool isValidIin(string iin)
        {
            return iin.Length == 12;
        }

        private static void writeToFile(List<Contractor> list)
        {
            try
            {
                /*
                 *  Формат для ип:
                    ИП "Название ип" биин 
                    Формат для физлиц:
                    Имя Фамилия Отчество иин гендер
                    -------------------------------
                    Отчество может отсутствовать
                    Длина иина должна быть 12 символов
                    Название ип без пробела должна быть
                 */
                using (StreamWriter sr = new StreamWriter("output.txt"))
                {
                    foreach (var contractor in list)
                    {
                        string data = "";
                        if (contractor is IndividContractor)
                        {
                            IndividContractor temp = (contractor as IndividContractor);
                            data = temp.Name + " " + temp.Surname + " " + temp.Patronymic + " " + temp.biin;
                        }
                        else
                        {
                            LegalContactor temp = (contractor as LegalContactor);
                            data = temp.CompanyName + " " + temp.biin;
                        }

                        sr.WriteLine(data);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("CAN NOT CREATE FILE");
            }
        }
    }
}