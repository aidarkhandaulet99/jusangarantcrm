using System;
using System.Collections.Generic;

namespace JusanGarantCrm
{
    public class LegalContactor : Contractor
    {
        public string CompanyName { get; set; }
        public List<IndividContractor> contacts{ get; set; }

        public LegalContactor(
            string companyName,
            string biin
        ) : base(biin)
        {
            CompanyName = companyName;
        }

        public void addContact(IndividContractor contact)
        {
            contacts.Add(contact);
            updatedDate = DateTime.Now;
        }

        public override ContractorType contractorType => ContractorType.LEGAL;
    }
}