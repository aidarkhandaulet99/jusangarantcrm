namespace JusanGarantCrm
{
    public class IndividContractor : Contractor
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Patronymic { get; set; }

        public IndividContractor(
            string name,
            string surname,
            string patronymic,
            string biin
        ) : base(biin)
        {
            Name = name;
            Surname = surname;
            Patronymic = patronymic;
        }

        public override ContractorType contractorType => ContractorType.INDIVIDUAL;
    }
}